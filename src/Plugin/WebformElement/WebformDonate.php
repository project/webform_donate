<?php

namespace Drupal\webform_donate\Plugin\WebformElement;

use Drupal\currency\Entity\Currency;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\media_entity\Entity\Media;

/**
 * Provides a 'webform_donate' element.
 *
 * @WebformElement(
 *   id = "webform_donate",
 *   label = @Translation("Donate"),
 *   category = @Translation("Webform Donate"),
 *   description = @Translation("Provides a form element to set the donation amount."),
 *   multiline = FALSE,
 *   composite = TRUE,
 *   states_wrapper = FALSE,
 * )
 */
class WebformDonate extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    // /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    // $currency = Currency::load('GBP');
    $amount = number_format(floatval($value['amount']), 2);

    $items = [];
    $frequency_value = $value['frequency'];
    if ($frequency_value == 'recurring') {
      $frequency_value = 'Monthly';
    }
    $items['donation_frequency'] = (string) $this->t('<em>Frequency:</em> @value', ['@value' => $frequency_value]);
    $items['donation_amount'] = (string) $this->t('<em>Amount:</em> @value', ['@value' => $amount]);

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);
    /** @var \Drupal\currency\Entity\CurrencyInterface $currency */
    // $currency = Currency::load('GBP');
    $amount = number_format(floatval($value['amount']), 2);

    $items = [];
    $items['donation_frequency'] = (string) $this->t('Frequency: @value', ['@value' => $value['frequency']]);
    $items['donation_amount'] = (string) $this->t('Amount: @value', ['@value' => $amount]);
    $items['donation_currency'] = (string) $this->t('Currency: @value', ['@value' => $value['currency']]);
    return $items;
  }

  /**
   * @return array
   */
  public function getDefaultProperties() {
    $frequencies = $this->getFrequencies();
    $currencies = $this->getCurrencies(TRUE);

    $defaults = [
      'default_approach_code' => '',
      'frequency_default' => '',
      'currency_default' => '',
      'euro' => FALSE,
    ];

    $frequency_defaults = [];
    foreach ($currencies as $currency => $currency_symbol) {
      foreach ($frequencies as $frequency_id => $frequency) {
        $frequency_defaults[$currency . '_' . $frequency_id . '_enabled'] = FALSE;
        $frequency_defaults[$currency . '_' . $frequency_id . '_option_label'] = $frequency;
        $frequency_defaults[$currency . '_' . $frequency_id . '_minimum_other'] = 1;

        for ($i = 0; $i < 3; $i++) {
          $frequency_defaults[$currency . '_' . $frequency_id . '_amount_' . $i] = '';
          $frequency_defaults[$currency . '_' . $frequency_id . '_benefit_' . $i] = '';
        }

        $frequency_defaults[$currency . '_' . $frequency_id . '_hero_image_enabled'] = FALSE;
        $frequency_defaults[$currency . '_' . $frequency_id . '_image_reference'] = '';
        $frequency_defaults[$currency . '_' . $frequency_id . '_image_style'] = '';
      }
    }

    $defaults += $frequency_defaults;

    return parent::getDefaultProperties() + $defaults;
  }

  /**
   * @return array
   */
  public static function getFrequencies($with_hash = FALSE) {
    $hash = $with_hash ? "#" : "";

    $frequencies = [
      $hash . 'one_off' => 'One off',
      $hash . 'recurring' => 'Recurring',
    ];

    return $frequencies;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $frequencies = $this->getFrequencies();
    $currencies = $this->getCurrencyOptions();

    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'Webform Donate Settings',
    ];

    $form['wrapper']['default_approach_code'] = [
      '#type' => 'textfield',
      '#title' => t('Default approach code'),
    ];

    $form['wrapper']['euro'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable Euros?'),
      '#default_value' => $this->configuration['EUR'],
    ];

    $form['wrapper']['frequency_default'] = [
      '#type' => 'select',
      '#title' => t('Default frequency'),
      '#options' => $frequencies,
      '#description' => t('Select which of the frequencies is the default'),
      '#default_value' => $this->configuration['frequency_default'],
    ];

    $form['wrapper']['currency_default'] = [
      '#type' => 'select',
      '#title' => t('Default currency'),
      '#options' => $currencies,
      '#description' => t('Select which of the currencies is the default'),
      '#default_value' => $this->configuration['currency_default'],
    ];

    foreach ($currencies as $currency => $currency_symbol) {
      foreach ($frequencies as $frequency_id => $frequency) {
        if ($currency == 'EUR') {
          $form['wrapper'][$currency][$frequency_id] = [
            '#type' => 'details',
            '#title' => t('@currency - Frequency - @frequency', array('@currency' => $currency_symbol, '@frequency' => $frequency)),
            '#open' => FALSE,
            '#states' => [
              'visible' => [
                ':input[name="properties[euro]"]' => ['checked' => TRUE],
              ],
            ],
          ];
        } else {
          $form['wrapper'][$currency][$frequency_id] = [
            '#type' => 'details',
            '#title' => t('@currency - Frequency - @frequency', array('@currency' => $currency_symbol, '@frequency' => $frequency)),
            '#open' => FALSE,
          ];
        }

        $form['wrapper'][$currency][$frequency_id][$currency . '_' . $frequency_id . '_enabled'] = [
          '#type' => 'checkbox',
          '#title' => t('Enabled'),
        ];

        $form['wrapper'][$currency][$frequency_id]['wrapper'] = [
          '#type' => 'fieldset',
        ];

        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_option_label'] = [
          '#type' => 'textfield',
          '#title' => t('Option label'),
        ];

        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_minimum_other'] = [
          '#type' => 'number',
          '#title' => t('Minimum other amount'),
        ];

        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_amounts'] = [
          '#type' => 'table',
          '#header' => [
            t('Amount'),
            t('Benefit'),
          ],
        ];

        $amount = 3;

        for ($i = 0; $i < $amount; $i++) {
          $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_amounts'][$i] = [
            $currency . '_' . $frequency_id . '_amount_' . $i => [
              '#type' => 'textfield',
              '#size' => 5,
            ],
            $currency . '_' . $frequency_id . '_benefit_' . $i => [
              '#type' => 'textfield',
              '#placeholder' => 'Text must not exceed 3 lines (c. 100 characters). This may take some trial and error...',
            ],
          ];
        }

        $media = NULL;
        if (!empty($this->configuration[$frequency_id]['hero_image']['image_reference'])) {
          $media = Media::load($this->configuration[$frequency_id]['hero_image']['image_reference']);
        }

        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_hero_image'] = [
          '#type' => 'details',
          '#title' => 'Hero image',
          '#open' => FALSE,
        ];

        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_hero_image'][$frequency_id . '_hero_image_enabled'] = [
          '#type' => 'checkbox',
          '#title' => t('Enabled'),
          '#default_value' => $this->configuration[$frequency_id]['hero_image']['enabled'],
          '#description' => t('Displays a hero image for one off payments in the donate widget form.'),
        ];
        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_hero_image'][$frequency_id . '_image_reference'] = [
          '#title' => t('Hero image'),
          '#type' => 'entity_autocomplete',
          '#target_type' => 'media',
          '#default_value' => $media,
          '#description' => t('Enter the name of the media image to display.'),
        ];
        $form['wrapper'][$currency][$frequency_id]['wrapper'][$currency . '_' . $frequency_id . '_hero_image'][$frequency_id . '_image_style'] = [
          '#title' => t('Image style'),
          '#type' => 'select',
          '#default_value' => $this->configuration[$frequency_id]['hero_image']['image_style'],
          '#options' => image_style_options(FALSE),
          '#description' => t('Select display style of media image.'),
        ];
      }
    }


    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Placeholder method for getting frequency/amounts settings.
   *
   * @todo replace with perhaps a trait for donation configuration.
   *
   * @return array
   *   An array of amounts by frequency and currency.
   */
  public static function getAmounts(array $frequencies, array $currencies, array $element) {
    $amounts_full = [];

    foreach ($currencies as $currency => $currency_symbol) {
      foreach ($frequencies as $frequency_id => $frequency) {
        if (!empty($element['#' . $currency . '_' . $frequency_id . '_enabled'])) {
          $amounts = [];

          for ($i = 0; $i < 6; $i++) {
            if (!empty($element['#' . $currency . '_' . $frequency_id . '_benefit_' . $i])) {
              $amounts[$element['#' . $currency . '_' . $frequency_id . '_amount_' . $i]] = [
                'benefit' => $element['#' . $currency . '_' . $frequency_id . '_benefit_' . $i],
                'label' => $element['#' . $currency . '_' . $frequency_id . '_amount_' . $i],
              ];
            }
          }
          $amounts_full[$currency][$frequency_id] = [
            'label' => !empty($element['#' . $currency . '_' . $frequency_id . '_option_label']) ? $element['#' . $currency . '_' . $frequency_id . '_option_label'] : $frequency,
            'amounts' => $amounts,
            'minimum_other' => !empty($element['#' . $currency . '_' . $frequency_id . '_minimum_other']) ? $element['#' . $currency . '_' . $frequency_id . '_minimum_other'] : FALSE,
            'allow_hero_image' => !empty($element['#' . $currency . '_' . $frequency_id . '_hero_image_enabled']) ? $element['#' . $currency . '_' . $frequency_id . '_hero_image_enabled'] : FALSE,
            'hero_image_reference' => !empty($element['#' . $currency . '_' . $frequency_id . '_image_reference']) ? $element['#' . $currency . '_' . $frequency_id . '_image_reference'] : FALSE,
            'hero_image_style' => !empty($element['#' . $currency . '_' . $frequency_id . '_image_style']) ? $element['#' . $currency . '_' . $frequency_id . '_image_style'] : FALSE,
          ];
        }
      }
    }

    return $amounts_full;
  }


  /**
   * @return array
  */
  public static function getCurrencies($euro = FALSE) {
    if ($euro) {
      $currencies = [
        "EUR" => '€',
        "GBP" => "£",
      ];
    } else {
      $currencies = ["GBP" => "£"];
    }

    return $currencies;
  }

  /**
   * @return array
   */
  public static function getCurrencyOptions() {
    $currencies = [
      "EUR" => '€',
      "GBP" => "£",
    ];

    return $currencies;
  }

  /**
   * Get defaults.
   *
   * @return array
   *   Array of defaults.
   */
  public static function getAmountDefaults(array $frequencies, array $element) {
    $amount_defaults = [];

    if (!empty($element['#frequency_default'])) {
      $amount_defaults['frequency_default'] = $element['#frequency_default'];
    }

    if (!empty($element['#currency_default'])) {
      $amount_defaults['currency_default'] = $element['#currency_default'];
    }

    return $amount_defaults;
  }

}

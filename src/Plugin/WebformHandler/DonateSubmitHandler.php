<?php

namespace Drupal\webform_donate\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Handler for webform donate submit.
 *
 * @WebformHandler(
 *   id = "donate_submit_handler",
 *   label = @Translation("Donate submit"),
 *   category = @Translation("Webform Donate"),
 *   description = @Translation("Configure redirect / posts to remote after submit."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class DonateSubmitHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = [
      'remote_base_url' => '',
      'remote_one_off_path' => '',
      'remote_recurring_path' => '',
      'approach_code' => '',
      'currency' => '',
      'behaviour' => 0,
    ];
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['behaviour'] = [
      '#type' => 'radios',
      '#title' => $this->t('Submit behaviour'),
      '#default_value' => $this->configuration['behaviour'],
      '#options' => array(
        0 => $this->t('Do nothing (off).'),
        1 => $this->t('Redirect (get) to Donate platform.'),
        2 => $this->t('Post (submit) to Donate platform.'),
      ),
    ];

    $form['remote_base_url'] = [
      '#type' => 'textfield',
      '#title' => t('Remote base url'),
      '#default_value' => $this->configuration['remote_base_url'],
    ];
    $form['remote_one_off_path'] = [
      '#type' => 'textfield',
      '#title' => t('Remote one-off path'),
      '#default_value' => $this->configuration['remote_one_off_path'],
    ];
    $form['remote_recurring_path'] = [
      '#type' => 'textfield',
      '#title' => t('Remote recurring path'),
      '#default_value' => $this->configuration['remote_recurring_path'],
    ];
    $form['one_off_approach_code'] = [
      '#type' => 'textfield',
      '#title' => t('One off approach code'),
      '#default_value' => $this->configuration['one_off_approach_code'],
    ];
    $form['recurring_approach_code'] = [
      '#type' => 'textfield',
      '#title' => t('Recurring approach code'),
      '#default_value' => $this->configuration['recurring_approach_code'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['remote_base_url'] = $values['remote_base_url'];
    $this->configuration['remote_one_off_path'] = $values['remote_one_off_path'];
    $this->configuration['remote_recurring_path'] = $values['remote_recurring_path'];
    $this->configuration['one_off_approach_code'] = $values['one_off_approach_code'];
    $this->configuration['recurring_approach_code'] = $values['recurring_approach_code'];
    $this->configuration['behaviour'] = $values['behaviour'];
  }

  /**
   * {@inheritdoc}
   */
  public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $values = $webform_submission->getData();
    if (count($values) >= 0) {
      $index = array_keys($values);
      $id = $index[0];

      $behaviour = (int) $this->configuration['behaviour'];

      if ($behaviour === 1) {
        $currency = !empty($values[$id]['currency']) ? $values[$id]['currency'] : "GBP";
        $frequency = $values[$id]['frequency'];

        $base_url = $this->configuration['remote_base_url'];
        if ($frequency == 'one_off') {
          $uri = $base_url . $this->configuration['remote_one_off_path'];
          $approach_code = $this->configuration['one_off_approach_code'];
        }
        else {
          $uri = $base_url . $this->configuration['remote_recurring_path'];
          $approach_code = $this->configuration['recurring_approach_code'];
        }

        $separator = '?';
        $uri .= $separator . 'ApproachCode=' . $approach_code;
        $separator = '&';

        // Determine which value to use:
        if (!empty($values[$id]['amount'])) {
          $donation_amount = $values[$id]['amount'];
        } else {
          $amount_key = 'amount_' . $currency . "_" . $frequency;
          $donation_amount = $values[$id][$amount_key]['radios'];
        }

        $uri .= $separator . 'amount=' . $donation_amount;

        if (!empty($currency)) {
          $uri .= $separator . 'currency=' . $currency;
        }

        $url = Url::fromUri($uri);
        $response = new TrustedRedirectResponse($url->toString());
        $form_state->setResponse($response);

      }
      else if ((int)$behaviour === 2) {
        // todo: build out post to Donate platform.
      }
    }
  }

}

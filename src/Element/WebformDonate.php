<?php

namespace Drupal\webform_donate\Element;

use Drupal\webform\Element\WebformCompositeBase;
use Drupal\webform_donate\Plugin\WebformElement\WebformDonate as WebformDonateElement;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\webform\Entity\Webform;
use Drupal\media_entity\Entity\Media;
use Drupal\currency\Entity\Currency;

/**
 * Provides a webform element for an donations amount element.
 *
 * @FormElement("webform_donate")
 */
class WebformDonate extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return parent::getInfo();
  }

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [];

    $elements['currency'] = [
      '#type' => 'radios',
      '#title' => t('Currency'),
      '#options' => '',
      '#weight' => 0,
    ];

    $elements['frequency'] = [
      '#type' => 'radios',
      '#title' => t('Frequency'),
      '#options' => '',
      '#weight' => 0,
    ];

    $elements['amount_one_off'] = [
      '#type' => 'textfield',
      '#title' => t('Amount'),
      '#weight' => 2,
    ];

    $elements['amount_recurring'] = [
      '#type' => 'textfield',
      '#title' => t('Amount'),
      '#weight' => 2,
    ];

    $elements['amount'] = [
      '#type' => 'number',
      '#title' => t('Amount'),
      '#weight' => 3,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function processWebformComposite(&$element, FormStateInterface $form_state, &$complete_form) {
    parent::processWebformComposite($element, $form_state, $complete_form);

    if (!empty($element['#webform']) && $webform = Webform::load($element['#webform'])) {

      $currencies = WebformDonateElement::getCurrencies(isset($element['#euro']) ? TRUE : FALSE);
      $frequencies = WebformDonateElement::getFrequencies(FALSE);
      $amounts = WebformDonateElement::getAmounts($frequencies, $currencies, $element);
      $amount_defaults = WebformDonateElement::getAmountDefaults($frequencies, $element);

      $element['amount']['#label_attributes']['class'] = ['donate-amount'];

      // Set currency options.
      $element['currency']['#options'] = $currencies;
      $element['currency']['#default_value'] = $amount_defaults['currency_default'];;
      if( count($currencies) == 1) {
        // Only 1 currency, so hide the option.
        $element['currency']['#access'] = FALSE;
      }

      // Set frequency options.
      if( !empty($amounts)) {
        $element['frequency'] = array_merge($element['frequency'], WebformDonate::getElementProperties($amounts['GBP']));
        $element['frequency']['#default_value'] = $amount_defaults['frequency_default'];
        $element['frequency']['#after_build'] = array('custom_process_frequency');
      }

      foreach ($amounts as $currency => $currency_details) {
        foreach ( $currency_details as $frequency_id => $frequency_details) {
          $benefits = WebformDonate::getElementProperties($frequency_details['amounts'], 'benefit');
          $form_state->set('benefits_' . $currency . '_' . $frequency_id, $benefits);
          $form_state->set('mininum_other_' . $currency . '_' . $frequency_id, $frequency_details['minimum_other']);
          $form_state->set('currencies', $currencies);
          $options = $benefits['#options'];
          $option_keys = array_keys($options);

          $element['amount_' . $currency . '_' . $frequency_id] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'donate__amounts',
                'donate__amounts--' . str_replace('_', '-', $frequency_id),
                'donate__amounts--currency--' . $currency,
                $amount_defaults['frequency_default'] == $frequency_id ? 'donate__amounts--default' : '',
              ],
            ],
            'radios' => array(
              '#type' => 'radios',
              '#options' => $options,
              '#default_value' => $options[$option_keys[0]],
              '#after_build' => array('custom_process_radios_' . $currency . '_' . $frequency_id),
            ),
          ];

          if (!empty($frequency_details['allow_hero_image']) && !empty($frequency_details['hero_image_reference'])) {
            $media_entity = Media::load($frequency_details['hero_image_reference']);
            if (!empty($media_entity)) {
              $element['amount_' . $currency . '_' . $frequency_id]['hero_image'] = [
                '#theme' => 'image_style',
                '#style_name' => $frequency_details['hero_image_style'],
                '#uri' => $media_entity->field_image->entity->getFileUri(),
                '#theme_wrappers' => [
                  'container' => [
                    '#attributes' => ['class' => []],
                  ],
                ],
                '#weight' => 10,
              ];
            }
          }

          $element['#element_validate'] = [
            [
              get_called_class(),
              'validateWebformDonate',
            ],
          ];
        }
      }
    }

    return $element;
  }

  /**
   * Get element properties from a settings array.
   *
   * @param array $settings
   *   Settings array.
   * @param string $description_key
   *   Key.
   *
   * @return array
   *   Return element properties array.
   */
  private static function getElementProperties(array $settings, $description_key = '') {
    $options = [];
    $descriptions = [];

    if (!empty($settings)) {
      $default_key = array_keys($settings)[0];
      foreach ($settings as $key => $details) {
        $options[$key] = $details['label'];

        if ($description_key) {
          $descriptions[$key] = !empty($details[$description_key]) ? $details[$description_key] : '';
        }
      }
    }

    return [
      '#options' => $options,
      '#button_descriptions' => $descriptions,
    ];
  }

  /**
   * Validates a webform_donate_amount element.
   */
  public static function validateWebformDonate(&$element, FormStateInterface $form_state, &$complete_form) {
    // Donation webform amount field must be converted into a single value.
    $frequency = $element['frequency']['#value'] ?: $element['frequency']['#default_value'];
    $frequency = trim($frequency);
    $form_state->set('frequency', $frequency);
    $values = $form_state->getValues();

    if (!empty($values['donate']['amount'])) {
      $amount = $values['donate']['amount'];
    }
    else {
      $amount = $values['donate']['amount_' . $frequency]['radios'];
    }

    $form_state->setValueForElement($element['amount'], $amount);
    $form_state->set('amount', $amount);

    return $element;
  }

}
